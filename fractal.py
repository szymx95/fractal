from PIL import Image
import numpy as np
import multiprocessing
import time
from multiprocessing import sharedctypes

import math
class point:
    def __init__(self,x,y):
        self.x=x
        self.y=y

    def __eq__(self,other):
        if self.x == other.x and self.y == other.y:
            return True
        return False
    def getPointDif(self,other):
        newPoint = point(self.y - other.x, self.x - other.y - 2)
        return newPoint

    def movePoint(self,other):
        newPoint = point( self.y - other.x, -self.x - other.y)
        return newPoint

def addPoints(startPoint,iterations,queue):
    fractal = [startPoint]
    
    minX =0
    maxX =10
    minY =0
    maxY =10

    lastPoint = point(1,1)
    newPoint  = point(5,1)
    for e in range(0,iterations):
        pointDif = lastPoint.getPointDif(newPoint)
        newPoint = newPoint.movePoint(pointDif)

        for z in range(0, len(fractal)):
            fractal.append( fractal[z].movePoint(pointDif) )
            
            minX = fractal[-1].x if fractal[-1].x < minX else minX
            maxX = fractal[-1].x if fractal[-1].x > maxX else maxX
            minY = fractal[-1].y if fractal[-1].y < minY else minY
            maxY = fractal[-1].y if fractal[-1].y > maxY else maxY

    data =np.ones((maxY-minY+10,maxX-minX+10), dtype=np.uint8)*255
    for e in fractal:
        data[e.y - minY + 5][e.x - minX + 5] = 0

    queue.put(data)

    print(startPoint.x,startPoint.y)

if __name__ == "__main__":

    iterations = int(input("number of iterations:"))
    start = time.time()
    q = multiprocessing.Queue()
    
    processes = [multiprocessing.Process(target = addPoints, args = (point(e,1),iterations,q) ) for e in range(1,6)]

    for e in processes:
        e.start()

    data = q.get()

    for _ in range(1,len(processes)):
        data = data * q.get()



    img = Image.fromarray(data)
    img.save('imgs/my.png')

    timeDifference = time.time() - start

    print("Elapsed time %d minutes and %d seconds" % (int(timeDifference / 60), timeDifference - 60*int(timeDifference / 60)))